# OpenSIC 

This repo provides a plain HTML group page for the OpenSIC project. 

[OpenSIC GitLab pages](https://opensic.gitlab.io/opensic/)

# Infos 

- HTML color picker 
    + https://htmlcolorcodes.com/

- public/index.html
    + main file 
    + plain html 
    + all-in-one; all pages are in one html document 

- public/style.css 
    + style definitions for index.html 

- Symbols 
    +   &#x1F3E0; -> ugly house
        &#9825; -> heart
        &#64; -> at
        &#9749; -> coeffee
        &#x1F3DB; -> old house
        &#8962; &#x2302; -> simple house
        &hearts;
        &#128154;
        &#9883;

- Contact: cards and profile 
    + https://www.foolishdeveloper.com/2021/09/simple-profile-card-ui-design.html
    + https://www.foolishdeveloper.com/2021/09/blog-post-card-design-using-html-css.html 
    + I basically used the blog card design to arrange the entries of the pages 
    + The contact profile cards are adjusted version of the profile-card-ui-design (card object) 
    + To have a full screen card a generated a inheritated object (card2 object)  
